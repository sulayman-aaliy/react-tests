import React from 'react';
import debounce from "lodash/debounce";
import "./autocomplete.css";

class AutoComplete extends React.Component {
    static number = 0;
    constructor (props) {
        super(props);
        this.id = AutoComplete.number++;
        let txt = (props.initialText || "").trim();
        this.state = { text: txt, realText: props.initialText || "", latest: null, isLoading: !!txt, shouldDisplaySuggestions: false, selectedValue: null, typedText: txt };
        this.debounceChangeText = debounce(this.changeText, 250);
        this.results = [];
    }

    render() {
        let hideClass = this.state.shouldDisplaySuggestions ? "" : "hide-it";
        return <div className={`autocomplete autocomplete${this.id}`}>
                <input type="text" value={this.state.typedText} onFocus={this.textFocus} onChange={this.textChange} onInput={this.textChanging} onKeyUp={this.keyTyped}></input>
                {this.state.isLoading && <span className={hideClass}>Loading ...</span>}
                {(!this.state.isLoading && this.results.length > 0) && this.renderResults(hideClass)}
                {(!this.state.isLoading && this.results.length === 0) && <span className={hideClass}>No result</span>}
            </div>;
    }
    
    componentDidMount() {
        if (this.state.text) {
            this.setState({ selectedValue: this.state.text });
        }
        document.addEventListener("click", event => {
            if (!this.state.shouldDisplaySuggestions) {
                return;
            }

            const selector = `.autocomplete${this.id}`;
            const elements = [document.querySelector(selector)];
            document.querySelectorAll(selector + " *").forEach((e, x) => elements.push(e));
            if (elements.indexOf(event.target) < 0) {
                if (this.props.emptyWhenNotFound) {
                    this.setState({ shouldDisplaySuggestions: false, typedText: "" });
                }
                else {
                    this.setState({ shouldDisplaySuggestions: false });
                }
            }
        });
    }

    textFocus = event => {
        document.querySelectorAll(`.autocomplete${this.id} li`).forEach((e, x) => e.classList.remove("highlighted"));
    };

    textChange = event => { };

    textChanging = event => {
        const t = event.target.value;
        this.setState({ typedText: t }, () => this.tryChangeText(t));
    };

    keyTyped = event => {
        if (event.defaultPrevented) {
            return; // Do nothing if the event was already processed
        }
        
        if (!this.state.shouldDisplaySuggestions) {
            return true;
        }
        const keyName = event.key.toLowerCase();
        if (keyName.indexOf("esc") === 0) {
            if (this.props.emptyWhenNotFound) {
                this.setState({ shouldDisplaySuggestions: false, typedText: "" });
            }
            else {
                this.setState({ shouldDisplaySuggestions: false });
            }
            event.preventDefault();
        }
        else if ((keyName === "down" || keyName === "arrowdown") && this.results.length > 0) {
            let li = document.querySelector(`.autocomplete${this.id} li.highlighted`);
            if (!li) {
                li = document.querySelector(`.autocomplete${this.id} li`);
            }
            else {
                if (li.nextElementSibling) {
                    li.classList.remove("highlighted");
                }
                li = li.nextElementSibling;
            }
            if (!li) {
                return true;
            }
            li.classList.add("highlighted");
            event.preventDefault();
        }
        else if ((keyName === "up" || keyName === "arrowup") && this.results.length > 0) {
            let li = document.querySelector(`.autocomplete${this.id} li.highlighted`);
            if (!li) {
                return true;
            }
            li.classList.remove("highlighted");
            li = li.previousElementSibling;
            if (!li) {
                return true;
            }
            li.classList.add("highlighted");
            event.preventDefault();
        }
        else if (keyName === "enter") {
            let li = document.querySelector(`.autocomplete${this.id} li.highlighted`);
            if (!li) {
                return true;
            }
            li.click();
            event.preventDefault();
        }
    };

    tryChangeText = t => {
        if (this.props.useLatestCache && this.state.latest && this.state.latest.text === t) {
            this.results = this.state.latest.results;
            this.setState({ text: t, realText: t, isLoading: false, shouldDisplaySuggestions: true, selectedValue: null });
        }
        else {
            this.setState({ isLoading: true, shouldDisplaySuggestions: true, selectedValue: null });
            this.debounceChangeText(t);
        }
    };

    changeText = txt => {
        this.results = [];
        this.setState({ text: txt.trim(), realText: txt, isLoading: true, shouldDisplaySuggestions: true, selectedValue: null }, this.search);
    };

    selectSuggestion = event => {
        const text = event.target.innerText;
        event.target.classList.remove("highlighted");
        this.setState({ shouldDisplaySuggestions: false, selectedValue: text, typedText: text });
    };

    hoverValue = event => {
        document.querySelectorAll(`.autocomplete${this.id} li`).forEach((e, x) => e.classList.remove("highlighted"));
        event.target.classList.add("highlighted");
    };

    search = () => {
        if (!this.state.text.trim()) {
            this.results = [];
            this.setState({ latest: { text: this.state.text, results: [] }, isLoading: false, shouldDisplaySuggestions: false, selectedValue: null });
            return;
        }

        if (typeof this.props.url === "string" && this.props.url.length > 0) {
            fetch(`${this.props.url}/${this.state.text}`).then(response => response.json()).then(data => {
                let array = JSON.parse(data);
                this.results = array.map(x => typeof x === "string" ? x : !x ? "" : (x.value || x.Value));
                let copy = [].concat(array);
                this.setState({ latest: { text: this.state.text, results: copy }, isLoading: false, shouldDisplaySuggestions: true, selectedValue: null });
            });
        }
        else if (Array.isArray(this.props.data)) {
            let array = this.props.data.filter(x => x.toLowerCase().indexOf(this.state.text.toLowerCase()) >= 0);
            let copy = [].concat(array);
            this.results = array;
            this.setState({ latest: { text: this.state.text, results: copy }, isLoading: false, shouldDisplaySuggestions: true, selectedValue: null });
        }
    }

    renderResults = (hideClass) => {
        return <div className={hideClass}><ol>
            {this.results.map((value, i) => {
                return <li key={value + i} onClick={this.selectSuggestion} onMouseOver={this.hoverValue}>{value}</li>;
            })}
        </ol></div>;
    };
}

AutoComplete.defaultProps = {
    url: "",
    data: ["Nassima", "Soul", "Adame", "Issam", "Jounayd"],
    initialText: "",
    emptyWhenNotFound: true,
    useLatestCache: false
};

export default AutoComplete;
