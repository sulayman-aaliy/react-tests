import React, { useRef, useState, useEffect, useCallback, useReducer } from "react";
import debounce from "lodash/debounce";
//import useDebounce from "../hooks/debounce";
import "./auto-drop-complete.css";

const initialState = { selectedValue: "" };
function reducer(state, action) {
    console.log(`${action.type} dispatched`);
    switch (action.type) {
        case "init-selection":
            return { selectedValue: action.initialValue };
        case "selection":
            return { selectedValue: action.value };
        default:
            throw new Error();
    }
}

function AutoDropComplete(props) {
    console.log("calling AutoDropComplete");
    const [text, setText] = useState((props.initialText || "").trim().toUpperCase());
    const [isLoading, setIsLoading] = useState(false);
    const [dropDown, setDropDown] = useState(false);
    const [typedText, setTypedText] = useState(props.initialText);
    const [filterResults, setFilterResults] = useState([]);
    const [state, dispatch] = useReducer(reducer, initialState);
    
    const referential = useRef(props.data || ["Nassima", "Soul", "Adame", "Issam", "Jounayd", "Guillermo", "Philippe"]);
    const id = useRef(Date.now());
    const isReady = useRef(false);
    const emptyWhenNotMatched = useRef(!!props.emptyWhenNotMatched);
    const css = useRef("auto-drop-complete");

    const debounceTextChange = useCallback(debounce(changeText, 300), [typedText]);
    function documentClick (event) {
        const selector = `.${css.current}${id.current}`;
        const elements = [document.querySelector(selector)];
        document.querySelectorAll(selector + " *").forEach((e, x) => elements.push(e));
        if (elements.indexOf(event.target) < 0) {
            if (emptyWhenNotMatched.current) {
                setDropDown(false);
                setTypedText("");
                setText("");
                setFilterResults(referential.current);
            }
            else {
                setDropDown(false);
            }
        }
    }

    useOnClickOutside(dropDown, documentClick);
    
    function changeText () {
        setText(typedText.trim().toUpperCase());
    }

    function textFocus (event) {
        setDropDown(true);
    }
    
    function textChange (event) {
        // do nothing onInput does eveything instead
        setDropDown(true);
    }

    function textChanging (event) {
        setTypedText(event.target.value);
    }

    function keyTyped (event) {
        if (event.defaultPrevented) {
            return; // Do nothing if the event was already processed
        }
        
        if (!dropDown) {
            return true;
        }
        const keyName = event.key.toLowerCase();
        const selector = `.${css.current}${id.current}`;
        const highlightCss = "highlighted";
        if (keyName.indexOf("esc") === 0) {
            if (emptyWhenNotMatched.current) {
                setText("");
                setTypedText("");
            }
            setDropDown(false);
            event.preventDefault();
        }
        else if ((keyName === "down" || keyName === "arrowdown") && filterResults.length > 0) {
            let li = document.querySelector(`${selector} li.${highlightCss}`);
            if (!li) {
                li = document.querySelector(`${selector} li`);
            }
            else {
                if (li.nextElementSibling) {
                    li.classList.remove(highlightCss);
                }
                li = li.nextElementSibling;
            }
            if (!li) {
                return true;
            }
            li.classList.add(highlightCss);
            event.preventDefault();
        }
        else if ((keyName === "up" || keyName === "arrowup") && filterResults.length > 0) {
            let li = document.querySelector(`${selector} li.${highlightCss}`);
            if (!li) {
                return true;
            }
            li.classList.remove(highlightCss);
            li = li.previousElementSibling;
            if (!li) {
                return true;
            }
            li.classList.add(highlightCss);
            event.preventDefault();
        }
        else if (keyName === "enter") {
            let li = document.querySelector(`${selector} li.${highlightCss}`);
            if (!li) {
                return true;
            }
            li.click();
            event.preventDefault();
        }
    }
    
    useEffect(() => {
        console.log("calling useEffect");
        function filter () {
            let results = referential.current;
            if (text) {
                results = referential.current.filter(x => x.toUpperCase().indexOf(text) >= 0);
            }
            setFilterResults(results);
        }

        if (isReady.current === null) {
            return;
        }

        if (isReady.current === false) {
            try {
                referential.current = referential.current.sort();
                filter();
                if (text) {
                    dispatch({ type: "init-selection", initialValue: text, key: id.current });
                }
                isReady.current = true;
            }
            catch (e) {
                console.error(e);
                isReady.current = null;
            }
        }
        else {
            if (text === typedText.trim().toUpperCase()) {
                if (dropDown) {
                    //console.log(`filter for: "${text}"`);
                    filter();
                }
            }
            else {
                debounceTextChange();
            }
        }
    }, [referential, id, css, dropDown, emptyWhenNotMatched, isReady, text, typedText, debounceTextChange]);

    function selectValue (event) {
        setText(event.target.innerText);
        setTypedText(event.target.innerText);
        setDropDown(false);
        dispatch({ type: "selection", value: event.target.innerText, key: id.current });

    }
    
    function hoverValue (event) {
        document.querySelectorAll(`.auto-drop-complete${id.current} li`).forEach((e, x) => e.classList.remove("highlighted"));
        event.target.classList.add("highlighted");
    }

    function cancelHoverValue (event) {
        event.target.classList.remove("highlighted");
    }

    function renderResults() {
        const hideClass = !isLoading && dropDown ? "" : "hide-it";
        return <div className={hideClass}><ol>
            {filterResults.length === 0 && <li className="ignore">Not found</li>}
            {filterResults.length > 0 && filterResults.map((value, i) => {
                return <li key={value + i} onClick={selectValue} onMouseOver={hoverValue} onMouseOut={cancelHoverValue}>{value}</li>;
            })}
        </ol></div>;
    }

    return <div className={`auto-drop-complete auto-drop-complete${id.current}`}>
                <input type="text" value={typedText} onFocus={textFocus} onChange={textChange} onInput={textChanging} onKeyUp={keyTyped}></input>
                {renderResults()}
            </div>;
}

function useOnClickOutside(ref, handler) {
    useEffect(() => {
        const click = event => {
          if (!ref) {
            return;
          }
  
          handler(event);
        };
  
        document.addEventListener('mousedown', click);
        document.addEventListener('touchstart', click);
  
        return () => {
          document.removeEventListener('mousedown', click);
          document.removeEventListener('touchstart', click);
        };
      },
      // Add ref and handler to effect dependencies
      // It's worth noting that because passed in handler is a new ...
      // ... function on every render that will cause this effect ...
      // ... callback/cleanup to run every render. It's not a big deal ...
      // ... but to optimize you can wrap handler in useCallback before ...
      // ... passing it into this hook.
      [ref, handler]
    );
}

export default AutoDropComplete;
