import React from 'react';
import logo from './logo.svg';
import './App.css';
import AutoComplete from "./components/AutoComplete/autocomplete";
import AutoDropComplete from "./components/AutoDropComplete/autodropcomplete";

function App() {
  return (
    <div className="App">
      <header className="App-header">

      <AutoDropComplete initialText="Guillermo" />

        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <AutoComplete initialText="Jounayd" />
      </header>
    </div>
  );
}

export default App;
