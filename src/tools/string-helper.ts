
export class StringHelper {
    ensureText = (obj?: string, defaultText: string = "", trimText: boolean = true) => {
        if (obj == null) return defaultText;
        let s = trimText ? obj.trim() : obj;
        return s || defaultText;
    }
}